import { writable, derived } from 'svelte/store';
import { navigateTo } from "yrv";

const AUTH_KEY = 'authentication';

const createAuthStore = () => {
  const { subscribe, set } = writable(null);

  const token = localStorage.getItem(AUTH_KEY);
  if (token && Date.now() - JSON.parse(token).expire < 0) {
    set(token);
  }

  function logout() {
    set(null);
    localStorage.removeItem(AUTH_KEY);
  }

  return {
    subscribe,
    getToken: () => {
      const authKey = localStorage.getItem(AUTH_KEY);
      if (authKey) {
        const auth = JSON.parse(authKey);
        if (Date.now() - auth.expire < 0) {
          return auth;
        }
      }
      logout();
      navigateTo("/unauthorized");
      return null;
    },
    setToken: (accessToken, expiresIn) => {
      const expire = Date.now() + expiresIn * 1000;
      const token = JSON.stringify({
        accessToken: accessToken,
        expire: expire
      });
      set(token);
      localStorage.setItem(AUTH_KEY, token);
    },
    logout: logout
  }
}

export const authStore = createAuthStore();
export const isLoggedIn = derived(authStore, $authStore => {
  return $authStore !== null
});

