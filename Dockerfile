FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./
RUN ["npm", "install"]

COPY . .
RUN ["npm", "run", "build"]

ENV PORT 9000
ENV API_ROOT "http://localhost"
ENV REDIRECT "http://localhost/redirect"
ENV CLIENT_ID ""
EXPOSE ${PORT}

CMD ["npm", "run", "serve"]
